-- A file by:
--  ___              _    _                 ___          _
-- | _ \ __ _  _ _  | |_ | | ___  ___ ___  / __| ___  __| | ___  _ _
-- |  _// _` || ' \ |  _|| |/ -_)(_-<(_-< | (__ / _ \/ _` |/ -_)| '_|
-- |_|  \__,_||_||_| \__||_|\___|/__//__/  \___|\___/\__,_|\___||_|
--
-- Nghia Lam (Pantless Coder)
-- https://codeberg.org/pantlesscoder/dotfiles

--
-- init.lua

if vim.fn.has('nvim-0.7') == 0 then
  error('Painless needs Neovim v0.7+ in order to run properly!!')
end

-- Run impatient plugin for optimization
do
  local success, impatient = pcall(require, 'impatient');
  if not success then
    vim.notify('impatient.nvim not installed!', vim.log.levels.WARN)
  else
    impatient.enable_profile()
  end
end

-- Initialize Painless modules
local modules = {
  'painless.core',
  'painless.plugins',
  'painless.utils',
  'painless.theme'
}

for _, module in ipairs(modules) do
  local ok, err = pcall(require, module)
  if not ok then
    vim.api.nvim_err_writeln("Failed to load " .. module .. "\n\n" .. err)
  end
end
