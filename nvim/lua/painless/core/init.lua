-- A file by:
--  ___              _    _                 ___          _
-- | _ \ __ _  _ _  | |_ | | ___  ___ ___  / __| ___  __| | ___  _ _
-- |  _// _` || ' \ |  _|| |/ -_)(_-<(_-< | (__ / _ \/ _` |/ -_)| '_|
-- |_|  \__,_||_||_| \__||_|\___|/__//__/  \___|\___/\__,_|\___||_|
--
-- Nghia Lam (Pantless Coder)
-- https://codeberg.org/pantlesscoder/dotfiles

--
-- core/init.lua

_G.painlessvim = {}

-- Global settings
--
painlessvim.compile_path = vim.fn.stdpath "config" .. "/lua/painless/_auto/compiled.lua"
painlessvim.user_terminals = {}

-- Core helper functions
--
function painlessvim.echo(messages)
  messages = messages or { { "\n" } }
  if type(messages) == "table" then
    vim.api.nvim_echo(messages, false, {})
  end
end

function painlessvim.compiled()
  local run_compile, _ = loadfile(painlessvim.compile_path)
  if run_compile then
    run_compile()
  else
    painlessvim.echo { { "Please run " }, { ":PackerSync", "Title" }}
  end
end

function painlessvim.set_binding(map_table, base)
  for mode, maps in pairs(map_table) do
    for keymap, options in pairs(maps) do
      if options then
        local cmd = options
        if type(options) == "table" then
          cmd = options[1]
          options[1] = nil
        else
          options = {}
        end
        vim.keymap.set(mode, keymap, cmd, vim.tbl_deep_extend("force", options, base or {}))
      end
    end
  end
end

function painlessvim.add_cmp_source(source)
  source = type(source) == "string" and { name = source } or source
  source.priority = ({
    nvim_lsp = 1000,
    luasnip = 750,
    buffer = 500,
    path = 250,
  })[source.name]

  local cmp_ok, cmp = pcall(require, "cmp")
  if cmp_ok then
    local config = cmp.get_config()
    table.insert(config.sources, source)
    cmp.setup(config)
  end
end

function painlessvim.toggle_term_cmd(term_details)
  if type(term_details) == "string" then term_details = { cmd = term_details, hidden = true } end
  local term_key = term_details.cmd
  if vim.v.count > 0 and term_details.count == nil then
    term_details.count = vim.v.count
    term_key = term_key .. vim.v.count
  end
  if painlessvim.user_terminals[term_key] == nil then
    painlessvim.user_terminals[term_key] = require("toggleterm.terminal").Terminal:new(term_details)
  end
  painlessvim.user_terminals[term_key]:toggle()
end
