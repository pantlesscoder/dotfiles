-- A file by:
--  ___              _    _                 ___          _
-- | _ \ __ _  _ _  | |_ | | ___  ___ ___  / __| ___  __| | ___  _ _
-- |  _// _` || ' \ |  _|| |/ -_)(_-<(_-< | (__ / _ \/ _` |/ -_)| '_|
-- |_|  \__,_||_||_| \__||_|\___|/__//__/  \___|\___/\__,_|\___||_|
--
-- Nghia Lam (Pantless Coder)
-- https://codeberg.org/pantlesscoder/dotfiles

--
-- plugins/init.lua

-- Init the packer
local packer = require('painless.plugins.packer').initialize()

-- Plugins
--
local function plugins(use)
  -- Plugins Manager
  use { "wbthomason/packer.nvim" }

  -- Optimiser
  use { "lewis6991/impatient.nvim" }

  -- Lua functions, requirement for telescope, neogit
  use { "nvim-lua/plenary.nvim", module = "plenary" }

  -- Neovim UI Enhancer
  use { "MunifTanjim/nui.nvim", module = "nui" }

  -- Popup API
  use { "nvim-lua/popup.nvim" }

  -- Quick Run
  use { "thinca/vim-quickrun" }

  -- Multiple cursor
  use { "mg979/vim-visual-multi" }

  -- Editor config
  use { "gpanders/editorconfig.nvim" }

  -- Better buffer closing
  use { "famiu/bufdelete.nvim", cmd = { "Bdelete", "Bwipeout" } }

  -- Notification enhancer
  use {
    "rcarriga/nvim-notify",
    event = "VimEnter",
    config = function()
      require("painless.plugins.configs.notify").setup()
    end,
  }

  -- Start screen
  use {
    "goolord/alpha-nvim",
    config = function()
      require("painless.plugins.configs.alpha").setup()
    end,
  }

  -- Syntax highlighting
  use {
    "nvim-treesitter/nvim-treesitter",
    run = ":TSUpdate",
    event = { "BufRead", "BufNewFile" },
    cmd = {
      "TSInstall",
      "TSInstallInfo",
      "TSInstallSync",
      "TSUninstall",
      "TSUpdate",
      "TSUpdateSync",
      "TSDisableAll",
      "TSEnableAll",
    },
    config = function()
      require("painless.plugins.configs.treesitter").setup()
    end,
  }

  -- Snippet collection
  use { "rafamadriz/friendly-snippets", opt = true }

  -- Snippet engine
  use {
    "L3MON4D3/LuaSnip",
    module = "luasnip",
    wants = "friendly-snippets",
    config = function()
      require("painless.plugins.configs.luasnip").setup()
    end,
  }

  -- Completion engine
  use {
    "hrsh7th/nvim-cmp",
    event = "InsertEnter",
    config = function()
      require("painless.plugins.configs.cmp").setup()
    end,
  }

  -- Snippet completion source
  use {
    "saadparwaiz1/cmp_luasnip",
    after = "nvim-cmp",
    config = function()
      painlessvim.add_cmp_source "luasnip"
    end,
  }

  -- Buffer completion source
  use {
    "hrsh7th/cmp-buffer",
    after = "nvim-cmp",
    config = function()
      painlessvim.add_cmp_source "buffer"
    end,
  }

  -- Path completion source
  use {
    "hrsh7th/cmp-path",
    after = "nvim-cmp",
    config = function()
      painlessvim.add_cmp_source "path"
    end,
  }

  -- LSP completion source
  use {
    "hrsh7th/cmp-nvim-lsp",
    after = "nvim-cmp",
    config = function()
      painlessvim.add_cmp_source "nvim_lsp"
    end,
  }

  -- Neovim LSP
  use { "neovim/nvim-lspconfig", event = { "VimEnter" } }

  -- LSP Manager
  use {
    "williamboman/nvim-lsp-installer",
    after = "nvim-lspconfig",
    config = function()
      require("painless.plugins.configs.lspinstaller").setup()
      require("painless.plugins.configs.lsp").setup()
    end,
  }

  -- LSP Symbols
  use {
    "stevearc/aerial.nvim",
    module = "aerial",
    cmd = { "AerialToggle", "AerialOpen", "AerialInfo" },
    config = function()
      require("painless.plugins.configs.aerial").setup()
    end,
  }

  -- Fuzzy finder
  use {
    "nvim-telescope/telescope.nvim",
    cmd = "Telescope",
    module = "telescope",
    config = function()
      require("painless.plugins.configs.telescope").setup()
    end,
  }

  use {
    ("nvim-telescope/telescope-%s-native.nvim"):format(vim.fn.has "win32" == 1 and "fzy" or "fzf"),
    after = "telescope.nvim",
    run = vim.fn.has "win32" ~= 1 and "make" or nil,
    config = function()
      require("telescope").load_extension(vim.fn.has "win32" == 1 and "fzy_native" or "fzf")
    end,
  }

  -- Terminal
  use {
    "akinsho/toggleterm.nvim",
    cmd = "ToggleTerm",
    module = { "toggleterm", "toggleterm.terminal" },
    config = function()
      require("painless.plugins.configs.toggleterm").setup()
    end,
  }

  -- Icons
  use {
    "kyazdani42/nvim-web-devicons",
    event = "VimEnter",
    config = function()
      require("painless.plugins.configs.icons").setup()
    end,
  }

  -- File tree
  use {
    "nvim-neo-tree/neo-tree.nvim",
    branch = "v2.x",
    module = "neo-tree",
    cmd = "Neotree",
    requires = { "MunifTanjim/nui.nvim", "nvim-lua/plenary.nvim" },
    setup = function()
      vim.g.neo_tree_remove_legacy_commands = true
    end,
    config = function()
      require("painless.plugins.configs.neotree").setup()
    end,
  }

  -- Status line
  use {
    "nvim-lualine/lualine.nvim",
    requires = { 'kyazdani42/nvim-web-devicons', opt = true },
    config = function()
      require("painless.plugins.configs.lualine").setup()
    end,
  }

  -- Splits
  use {
    "mrjones2014/smart-splits.nvim",
    module = "smart-splits",
    config = function()
      require("painless.plugins.configs.splits").setup()
    end,
  }

  -- Better escape
  use {
    "max397574/better-escape.nvim",
    event = "InsertCharPre",
    config = function()
      require("painless.plugins.configs.escape").setup()
    end,
  }

  -- Note taking
  use {
    "renerocksai/telekasten.nvim",
    requires = "telescope.nvim",
    config = function()
      require("painless.plugins.configs.note").setup()
    end,
  }

  use { "renerocksai/calendar-vim" }

  -- Async Run & Tasks system
  use { "skywind3000/asyncrun.vim" }
  use {
    "skywind3000/asynctasks.vim",
    config = function()
      require("painless.plugins.configs.asynctasks").setup()
    end,
  }
  use { "GustavoKatel/telescope-asynctasks.nvim" }

  -- LANGUAGEs
  -- ---------------------------------------------------

  -- Markdown support

  use({
    "iamcco/markdown-preview.nvim",
    run = function() vim.fn["mkdp#util#install"]() end,
  })

end

packer.startup(plugins)
painlessvim.compiled()
