-- A file by:
--  ___              _    _                 ___          _
-- | _ \ __ _  _ _  | |_ | | ___  ___ ___  / __| ___  __| | ___  _ _
-- |  _// _` || ' \ |  _|| |/ -_)(_-<(_-< | (__ / _ \/ _` |/ -_)| '_|
-- |_|  \__,_||_||_| \__||_|\___|/__//__/  \___|\___/\__,_|\___||_|
--
-- Nghia Lam (Pantless Coder)
-- https://codeberg.org/pantlesscoder/dotfiles

--
-- plugins/packer.lua

local M = {}

M.initialize = function ()
  -- Check for packer
  local succeed, packer = pcall(require, "packer")
  if not succeed then
    local packer_path = vim.fn.stdpath "data" .. "/site/pack/packer/start/packer.nvim"
    vim.fn.delete(packer_path, "rf")
    vim.fn.system {
      "git",
      "clone",
      "--depth",
      "1",
      "https://github.com/wbthomason/packer.nvim",
      packer_path,
    }

    painlessvim.echo { { "Initializing Packer...\n\n" } }
    vim.cmd "packadd packer.nvim"
    succeed, packer = pcall(require, "packer")
    if succeed then
        painlessvim.echo { { "Finished cloning Packer!\n" } }
    else
      vim.api.nvim_err_writeln("Failed to load packer at:" .. packer_path .. "\n\n" .. packer)
    end
  end

  -- Packer configurations
  local conf = {
    compile_path = painlessvim.compile_path,
    display = {
      open_fn = function()
        return require("packer.util").float { border = "rounded" }
      end,
    },
    profile = {
      enable = true,
      threshold = 0.0001,
    },
    git = {
      clone_timeout = 300,
      subcommands = {
        update = "pull --rebase",
      },
    },
    auto_clean = true,
    compile_on_sync = true,
  }

  -- Init
  packer.init(conf)

  return packer
end

M.check = function (plugin)
  return packer_plugins ~= nil and packer_plugins[plugin] ~= nil
end

return M
