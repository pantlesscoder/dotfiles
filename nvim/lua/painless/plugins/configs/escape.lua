-- A file by:
--  ___              _    _                 ___          _
-- | _ \ __ _  _ _  | |_ | | ___  ___ ___  / __| ___  __| | ___  _ _
-- |  _// _` || ' \ |  _|| |/ -_)(_-<(_-< | (__ / _ \/ _` |/ -_)| '_|
-- |_|  \__,_||_||_| \__||_|\___|/__//__/  \___|\___/\__,_|\___||_|
--
-- Nghia Lam (Pantless Coder)
-- https://codeberg.org/pantlesscoder/dotfiles

--
-- plugins/configs/escape.lua

local M = {}

M.setup = function ()
  local status_ok, escape = pcall(require, "better-escape")
  if not status_ok then
    return
  end

  escape.setup({
      mapping = {"jk"},           -- a table with mappings to use
      timeout = vim.o.timeoutlen, -- the time in which the keys must be hit in ms. Use option timeoutlen by default
      clear_empty_lines = false,  -- clear line after escaping if there is only whitespace
      keys = "<Esc>",             -- keys used for escaping, if it is a function will use the result everytime
      -- example(recommended)
      -- keys = function()
      --   return vim.api.nvim_win_get_cursor(0)[2] > 1 and '<esc>l' or '<esc>'
      -- end,
  })
end

return M
