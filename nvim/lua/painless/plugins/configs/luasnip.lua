-- A file by:
--  ___              _    _                 ___          _
-- | _ \ __ _  _ _  | |_ | | ___  ___ ___  / __| ___  __| | ___  _ _
-- |  _// _` || ' \ |  _|| |/ -_)(_-<(_-< | (__ / _ \/ _` |/ -_)| '_|
-- |_|  \__,_||_||_| \__||_|\___|/__//__/  \___|\___/\__,_|\___||_|
--
-- Nghia Lam (Pantless Coder)
-- https://codeberg.org/pantlesscoder/dotfiles

--
-- plugins/configs/luasnip.lua

local M = {}

M.setup = function ()
  local luasnip_ok, luasnip = pcall(require, "luasnip")
  local loader_ok, loader = pcall(require, "luasnip/loaders/from_vscode")
  if not (luasnip_ok and loader_ok)then
    return
  end

  loader.lazy_load()

  -- Custom extend
  luasnip.filetype_extend("cpp", {"unreal"})   -- Unreal engine snippet

end

return M
