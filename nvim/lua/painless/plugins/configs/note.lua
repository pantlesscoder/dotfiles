-- A file by:
--  ___              _    _                 ___          _
-- | _ \ __ _  _ _  | |_ | | ___  ___ ___  / __| ___  __| | ___  _ _
-- |  _// _` || ' \ |  _|| |/ -_)(_-<(_-< | (__ / _ \/ _` |/ -_)| '_|
-- |_|  \__,_||_||_| \__||_|\___|/__//__/  \___|\___/\__,_|\___||_|
--
-- Nghia Lam (Pantless Coder)
-- https://codeberg.org/pantlesscoder/dotfiles

--
-- plugins/configs/note.lua

local M = {}

M.setup = function ()
  local status_ok, telekasten = pcall(require, "telekasten")
  if not status_ok then
    return
  end

  local home = vim.fn.expand("~/Dropbox/notes")

  telekasten.setup({
    home = home,
    take_over_my_home = true,
    auto_set_filetype = true,

    -- Dir names 
    dailies      = home .. '/' .. 'daily',
    weeklies     = home .. '/' .. 'weekly',
    templates    = home .. '/' .. '__templates',
    image_subdir = "_img",

    extension    = ".md",

    new_note_filename = "title",

    template_new_note   = home .. '/' .. '__templates/note.md',
    template_new_daily  = home .. '/' .. '__templates/daily.md',
    template_new_weekly = home .. '/' .. '__templates/weekly.md',

    plug_into_calendar = true,
    calendar_opts = {
        weeknm = 4,
        calendar_monday = 1,
        calendar_mark = 'left-fit',
    },

    subdirs_in_links = true,
    rename_update_links = true,
    new_note_location = "same_as_current",

    -- Telescope actions behavior
    close_after_yanking = false,
    insert_after_inserting = true,

    -- Tag notation: '#tag', ':tag:', 'yaml-bare'
    tag_notation = "#tag",

    -- Command palette theme: dropdown (window) or ivy (bottom panel)
    command_palette_theme = "dropdown",

    -- tag list theme:
    show_tags_theme = "dropdown",
  })
end

return M
