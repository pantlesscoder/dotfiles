-- A file by:
--  ___              _    _                 ___          _
-- | _ \ __ _  _ _  | |_ | | ___  ___ ___  / __| ___  __| | ___  _ _
-- |  _// _` || ' \ |  _|| |/ -_)(_-<(_-< | (__ / _ \/ _` |/ -_)| '_|
-- |_|  \__,_||_||_| \__||_|\___|/__//__/  \___|\___/\__,_|\___||_|
--
-- Nghia Lam (Pantless Coder)
-- https://codeberg.org/pantlesscoder/dotfiles

--
-- plugins/configs/asynctasks.lua

local M = {}

M.setup = function ()
  local _, asynctasks = pcall(require, "asynctasks.vim")

  vim.cmd([[
let g:asynctasks_config_name = ['.tasks', '.git/tasks.ini', '.svn/tasks.ini', '.git/scripts/tasks.ini']
  ]])
end

return M
