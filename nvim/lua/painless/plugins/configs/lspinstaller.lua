-- A file by:
--  ___              _    _                 ___          _
-- | _ \ __ _  _ _  | |_ | | ___  ___ ___  / __| ___  __| | ___  _ _
-- |  _// _` || ' \ |  _|| |/ -_)(_-<(_-< | (__ / _ \/ _` |/ -_)| '_|
-- |_|  \__,_||_||_| \__||_|\___|/__//__/  \___|\___/\__,_|\___||_|
--
-- Nghia Lam (Pantless Coder)
-- https://codeberg.org/pantlesscoder/dotfiles

--
-- plugins/configs/lspinstaller.lua

local M = {}

M.setup = function ()
  local status_ok, lspinstaller = pcall(require, "nvim-lsp-installer")
  if not status_ok then
    return
  end

  lspinstaller.setup({
      ui = {
        icons = {
          server_installed = "✓",
          server_uninstalled = "✗",
          server_pending = "⟳",
        },
      },
  })
end

return M
