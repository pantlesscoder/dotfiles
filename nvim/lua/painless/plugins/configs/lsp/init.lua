-- A file by:
--  ___              _    _                 ___          _
-- | _ \ __ _  _ _  | |_ | | ___  ___ ___  / __| ___  __| | ___  _ _
-- |  _// _` || ' \ |  _|| |/ -_)(_-<(_-< | (__ / _ \/ _` |/ -_)| '_|
-- |_|  \__,_||_||_| \__||_|\___|/__//__/  \___|\___/\__,_|\___||_|
--
-- Nghia Lam (Pantless Coder)
-- https://codeberg.org/pantlesscoder/dotfiles

--
-- plugins/configs/lsp/init.lua

-- Painless LSP
--

painlessvim.lsp = {}

function painlessvim.lsp.on_attach(client, bufnr)
  -- Setup LSP aerial
  local aerial_ok, aerial = pcall(require, "aerial")
  if aerial_ok then
    aerial.on_attach(client, bufnr)
  end
end

function painlessvim.lsp.disable_formatting(client)
  client.resolved_capabilities.document_formatting = false
end

local M = {}

M.setup = function ()
  local status_ok, lsp = pcall(require, "lspconfig")
  if not status_ok then
    return
  end

  local signs = {
    { name = "DiagnosticSignError", text = "" },
    { name = "DiagnosticSignWarn", text = "" },
    { name = "DiagnosticSignHint", text = "" },
    { name = "DiagnosticSignInfo", text = "" },
  }

  for _, sign in ipairs(signs) do
    vim.fn.sign_define(sign.name, { texthl = sign.name, text = sign.text, numhl = "" })
  end

  vim.diagnostic.config({
      virtual_text = true,
      signs = { active = signs },
      update_in_insert = true,
      underline = true,
      severity_sort = true,
      float = {
        focusable = false,
        style = "minimal",
        border = "rounded",
        source = "always",
        header = "",
        prefix = "",
      },
  })

  vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, { border = "rounded" })
  vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, { border = "rounded" })

  local servers = {}
  local installer_ok, lsp_installer = pcall(require, "nvim-lsp-installer")
  if installer_ok then
    for _, server in ipairs(lsp_installer.get_installed_servers()) do
      table.insert(servers, server.name)
    end
  end

  -- Setup each servers
  for _, server in ipairs(servers) do
    local opts = {
      "lsp.servers." .. server,
      on_attach = painlessvim.lsp.on_attach,
    }
    lsp[server].setup(opts)
  end
end

return M
