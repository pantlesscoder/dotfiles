-- A file by:
--  ___              _    _                 ___          _
-- | _ \ __ _  _ _  | |_ | | ___  ___ ___  / __| ___  __| | ___  _ _
-- |  _// _` || ' \ |  _|| |/ -_)(_-<(_-< | (__ / _ \/ _` |/ -_)| '_|
-- |_|  \__,_||_||_| \__||_|\___|/__//__/  \___|\___/\__,_|\___||_|
--
-- Nghia Lam (Pantless Coder)
-- https://codeberg.org/pantlesscoder/dotfiles

--
-- theme/treesitter.lua

local treesitter = {
  TSError = { fg = C.red },
  TSPunctDelimiter = { fg = C.fg },
  TSPunctBracket = { fg = C.fg },
  TSPunctSpecial = { fg = C.fg },
  TSConstant = { fg = C.yellow },
  TSConstBuiltin = { fg = C.orange },
  TSConstMacro = { fg = C.red },
  TSStringRegex = { fg = C.yellow },
  TSString = { fg = C.yellow },
  TSStringEscap = { fg = C.red },
  TSCharacter = { fg = C.green },
  TSNumber = { fg = C.orange },
  TSBoolean = { fg = C.orange },
  TSFloat = { fg = C.green },
  TSAnnotation = { fg = C.yellow },
  TSAttribute = { fg = C.fg },
  TSNamespace = { fg = C.purple },
  TSFuncBuiltin = { fg = C.blue },
  TSFunction = { fg = C.red },
  TSFuncMacro = { fg = C.yellow },
  TSParameter = { fg = C.fg },
  TSParameterReference = { fg = C.cyan },
  TSMethod = { fg = C.red },
  TSField = { fg = C.fg },
  TSProperty = { fg = C.yellow },
  TSConstructor = { fg = C.fg },
  TSConditional = { fg = C.green },
  TSRepeat = { fg = C.green },
  TSLabel = { fg = C.blue },
  TSKeyword = { fg = C.green },
  TSKeywordFunction = { fg = C.green },
  TSKeywordOperator = { fg = C.green_1 },
  TSOperator = { fg = C.green_1 },
  TSException = { fg = C.purple },
  TSType = { fg = C.blue },
  TSTypeBuiltin = { fg = C.blue },
  TSStructure = { fg = C.green_1 },
  TSInclude = { fg = C.purple },
  TSVariable = { fg = C.fg },
  TSVariableBuiltin = { fg = C.yellow },
  TSText = { fg = C.fg },
  TSTextReference = { fg = C.yellow },
  TSStrong = { fg = C.blue, style = "bold" },
  TSEmphasis = { fg = C.green, style = "italic" },
  TSUnderline = { fg = C.fg },
  TSTitle = { fg = C.fg },
  TSLiteral = { fg = C.fg },
  TSURI = { fg = C.green },
  TSTag = { fg = C.red },
  TSTagDelimiter = { fg = C.fg },
  markdownTSNone = { fg = C.fg },
  markdownTSTitle = { fg = C.red },
  markdownTSLiteral = { fg = C.green },
  markdownTSPunctSpecial = { fg = C.red },
  markdownTSPunctDelimiter = { fg = C.fg },
}

return treesitter
