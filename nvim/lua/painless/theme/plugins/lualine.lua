return {
  normal = {
    a = { bg = C.green, fg = C.black, gui = 'bold' },
    b = { bg = C.fg, fg = C.white },
    c = { bg = C.fg, fg = C.bg },
    x = { bg = C.green_1, fg = C.white }
  },
  insert = {
    a = { bg = C.blue, fg = C.black, gui = 'bold' },
    b = { bg = C.fg, fg = C.white },
    c = { bg = C.fg, fg = C.bg },
    x = { bg = C.green_1, fg = C.white }
  },
  visual = {
    a = { bg = C.yellow, fg = C.black, gui = 'bold' },
    b = { bg = C.fg, fg = C.white },
    c = { bg = C.fg, fg = C.bg },
    x = { bg = C.green_1, fg = C.white }
  },
  replace = {
    a = { bg = C.red, fg = C.black, gui = 'bold' },
    b = { bg = C.fg, fg = C.white },
    c = { bg = C.fg, fg = C.bg },
    x = { bg = C.green_1, fg = C.white }
  },
  command = {
    a = { bg = C.green, fg = C.black, gui = 'bold' },
    b = { bg = C.fg, fg = C.white },
    c = { bg = C.fg, fg = C.bg },
    x = { bg = C.green_1, fg = C.white }
  },
  inactive = {
    a = { bg = C.black_1, fg = C.fg, gui = 'bold' },
    b = { bg = C.black_1, fg = C.fg },
    c = { bg = C.black_1, fg = C.fg }
  }
}
