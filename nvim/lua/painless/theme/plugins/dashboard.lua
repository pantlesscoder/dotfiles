return {
  DashboardHeader = { fg = C.fg },
  DashboardShortcut = { fg = C.yellow },
  DashboardFooter = { fg = C.cyan },
  DashboardCenter = { fg = C.blue },
}
