-- A file by:
--  ___              _    _                 ___          _
-- | _ \ __ _  _ _  | |_ | | ___  ___ ___  / __| ___  __| | ___  _ _
-- |  _// _` || ' \ |  _|| |/ -_)(_-<(_-< | (__ / _ \/ _` |/ -_)| '_|
-- |_|  \__,_||_||_| \__||_|\___|/__//__/  \___|\___/\__,_|\___||_|
--
-- Nghia Lam (Pantless Coder)
-- https://codeberg.org/pantlesscoder/dotfiles

--
-- theme/init.lua

-- Default settings
if vim.fn.exists "syntax_on" then 
  vim.cmd "syntax reset" 
end
vim.cmd "highlight clear"
vim.o.background = "dark"
vim.o.termguicolors = true
vim.g.colors_name = "painless_theme"

-- Load colors
_, C = pcall(require, 'painless.theme.colors')
if not C then
  return
end

local highlights = {}

for _, module in ipairs { "base", "treesitter", "lsp" } do
  highlights = vim.tbl_deep_extend("force", highlights, require("painless.theme." .. module))
end

for plugin, enabled in
  pairs({
    aerial = true,
    dashboard = true,
    highlighturl = true,
    neotree = true,
    notify = true,
    devicons = true,
  })
do
  if enabled then
    highlights = vim.tbl_deep_extend("force", highlights, require("painless.theme.plugins." .. plugin)) 
  end
end

local utils = require "painless.theme.utils"
for group, spec in pairs(highlights) do
  vim.api.nvim_set_hl(0, group, utils.parse_style(spec))
end

-- Lualine
local lualine_ok, lualine = pcall(require, 'lualine')
if not lualine_ok then
  return
end

local lualine_theme = require "painless.theme.plugins.lualine"
lualine.setup {
  options = { theme = lualine_theme }
}
