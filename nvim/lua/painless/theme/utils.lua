-- A file by:
--  ___              _    _                 ___          _
-- | _ \ __ _  _ _  | |_ | | ___  ___ ___  / __| ___  __| | ___  _ _
-- |  _// _` || ' \ |  _|| |/ -_)(_-<(_-< | (__ / _ \/ _` |/ -_)| '_|
-- |_|  \__,_||_||_| \__||_|\___|/__//__/  \___|\___/\__,_|\___||_|
--
-- Nghia Lam (Pantless Coder)
-- https://codeberg.org/pantlesscoder/dotfiles

--
-- theme/utils.lua

local M = {}

function M.parse_style(spec)
  if spec.style then
    for match in (spec.style .. ","):gmatch "(.-)," do
      spec[match] = true
    end
    spec.style = nil
  end
  return spec
end

return M
