-- A file by:
--  ___              _    _                 ___          _
-- | _ \ __ _  _ _  | |_ | | ___  ___ ___  / __| ___  __| | ___  _ _
-- |  _// _` || ' \ |  _|| |/ -_)(_-<(_-< | (__ / _ \/ _` |/ -_)| '_|
-- |_|  \__,_||_||_| \__||_|\___|/__//__/  \___|\___/\__,_|\___||_|
--
-- Nghia Lam (Pantless Coder)
-- https://codeberg.org/pantlesscoder/dotfiles

--
-- utils/init.lua

local utils_modules = {
  'painless.utils.settings',
  'painless.utils.binding',
}

for _, module in ipairs(utils_modules) do
  local ok, err = pcall(require, module)
  if not ok then
    vim.api.nvim_err_writeln("Failed to load " .. module .. "\n\n" .. err)
  end
end
