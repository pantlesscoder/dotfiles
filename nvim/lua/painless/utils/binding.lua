-- A file by:
--  ___              _    _                 ___          _
-- | _ \ __ _  _ _  | |_ | | ___  ___ ___  / __| ___  __| | ___  _ _
-- |  _// _` || ' \ |  _|| |/ -_)(_-<(_-< | (__ / _ \/ _` |/ -_)| '_|
-- |_|  \__,_||_||_| \__||_|\___|/__//__/  \___|\___/\__,_|\___||_|
--
-- Nghia Lam (Pantless Coder)
-- https://codeberg.org/pantlesscoder/dotfiles

--
-- utils/binding.lua

local maps   = { n = {}, i = {}, v = {}, t = {}, [""] = {} }
local packer = require('painless.plugins.packer')

-- My helper commands
vim.api.nvim_create_user_command(
  'Reload',
  "source $MYVIMRC",
  { bang = true }
)

-- Remove space functionality for setting leader key.
maps[""]["<Space>"] = "<Nop>"

-- NORMAL --
--

-- Basic
maps.n["H"] = { "^", desc = "Begin line of text" }
maps.n["L"] = { "$", desc = "End line of text" }

-- Buffer operators
maps.n["<leader>bs"] = { "<cmd>w<cr>", desc = "Buffer save" }
maps.n["<leader>bn"] = { "<cmd>bnext<cr>", desc = "Next buffer" }
maps.n["<leader>bp"] = { "<cmd>bprevious<cr>", desc = "Previous buffer" }
if packer.check("bufdelete.nvim") then
  maps.n["<leader>bk"] = { "<cmd>Bdelete<cr>", desc = "Buffer kill" }
else
  maps.n["<leader>bk"] = { "<cmd>bdelete<cr>", desc = "Buffer kill" }
end

-- Window operators
maps.n["<leader>wv"] = { "<cmd>vsplit<cr>", desc = "Window splits vertically" }
maps.n["<leader>ws"] = { "<cmd>split<cr>", desc = "Window splits horizontally" }
if packer.check("smart-splits.nvim") then
  -- Better window navigation
  maps.n["<leader>wh"] = {
    function()
      require("smart-splits").move_cursor_left()
    end,
    desc = "Move to left window",
  }
  maps.n["<leader>wj"] = {
    function()
      require("smart-splits").move_cursor_down()
    end,
    desc = "Move to below window",
  }
  maps.n["<leader>wk"] = {
    function()
      require("smart-splits").move_cursor_up()
    end,
    desc = "Move to above window",
  }
  maps.n["<leader>wl"] = {
    function()
      require("smart-splits").move_cursor_right()
    end,
    desc = "Move to right window",
  }

  -- Resize with arrows
  maps.n["<C-Up>"] = {
    function()
      require("smart-splits").resize_up()
    end,
    desc = "Resize window up",
  }
  maps.n["<C-Down>"] = {
    function()
      require("smart-splits").resize_down()
    end,
    desc = "Resize window down",
  }
  maps.n["<C-Left>"] = {
    function()
      require("smart-splits").resize_left()
    end,
    desc = "Resize window left",
  }
  maps.n["<C-Right>"] = {
    function()
      require("smart-splits").resize_right()
    end,
    desc = "Resize window right",
  }
else
  maps.n["<leader>wh"] = { "<C-w>h", desc = "Move to left window" }
  maps.n["<leader>wj"] = { "<C-w>j", desc = "Move to below window" }
  maps.n["<leader>wk"] = { "<C-w>k", desc = "Move to above window" }
  maps.n["<leader>wl"] = { "<C-w>l", desc = "Move to right window" }
  maps.n["<C-Up>"] = { "<cmd>resize -2<CR>", desc = "Resize window up" }
  maps.n["<C-Down>"] = { "<cmd>resize +2<CR>", desc = "Resize window down" }
  maps.n["<C-Left>"] = { "<cmd>vertical resize -2<CR>", desc = "Resize window left" }
  maps.n["<C-Right>"] = { "<cmd>vertical resize +2<CR>", desc = "Resize window right" }
end

-- Alpha
if packer.check("alpha-nvim") then
  maps.n["<leader>d"] = { "<cmd>Alpha<cr>", desc = "Alpha Dashboard" }
end

-- Packer
maps.n["<leader>ps"] = { "<cmd>PackerSync<cr>", desc = "Packer Sync" }
maps.n["<leader>pc"] = { "<cmd>PackerCompile<cr>", desc = "Packer Compile" }
maps.n["<leader>pi"] = { "<cmd>PackerInstall<cr>", desc = "Packer Install" }
maps.n["<leader>pS"] = { "<cmd>PackerStatus<cr>", desc = "Packer Status" }
maps.n["<leader>pu"] = { "<cmd>PackerUpdate<cr>", desc = "Packer Update" }

-- File tree
if packer.check("neo-tree.nvim") then
  maps.n["<leader>op"] = { "<cmd>Neotree toggle<cr>", desc = "Toggle Tree Explorer" }
  maps.n["<leader>oo"] = { "<cmd>Neotree focus<cr>", desc = "Focus Tree Explorer" }
end

-- LSP
if packer.check("nvim-lspconfig") then
  maps.n["gh"] = { function() vim.lsp.buf.hover() end, desc = "Hover symbol details" }
  maps.n["gd"] = { function() vim.lsp.buf.definition() end, desc = "Show the definition of current symbol" }
  maps.n["gD"] = { function() vim.lsp.buf.declaration() end, desc = "Declaration of current symbol" }
  maps.n["gI"] = { function() vim.lsp.buf.implementation() end, desc = "Implementation of current symbol" }
  maps.n["gr"] = { function() vim.lsp.buf.references() end, desc = "References of current symbol" }
  maps.n["gl"] = { function() vim.diagnostic.open_float() end, desc = "Hover diagnostics" }
  maps.n["]d"] = { function() vim.diagnostic.goto_next() end, desc = "Next diagnostic" }
  maps.n["[d"] = { function() vim.diagnostic.goto_prev() end, desc = "Previous diagnostics" }
  maps.n["<leader>ca"] = { function() vim.lsp.buf.code_action() end, desc = "LSP code action" }
  maps.n["<leader>cf"] = { function() vim.lsp.buf.formatting_sync() end, desc = "LSP format code" }
  maps.n["<leader>ch"] = { function() vim.lsp.buf.signature_help() end, desc = "Signature help" }
  maps.n["<leader>cr"] = { function() vim.lsp.buf.rename() end, desc = "Rename current symbol" }
end

if packer.check("aerial.nvim") then
  maps.n["<leader>co"] = { "<cmd>AerialToggle<cr>", desc = "Symbols outline" }
end

-- Telescope
if packer.check("telescope.nvim") then
  maps.n["<leader>:"] = { function() require("telescope.builtin").commands() end, desc = "Commands" }
  maps.n["<leader>sb"] = { function() require("telescope.builtin").live_grep() end, desc = "Search buffer" }
  maps.n["<leader>sB"] = {
    function()
      require("telescope.builtin").live_grep
      {
        additional_args = function(args) return vim.list_extend(args, { "--hidden", "--no-ignore" }) end,
      }
    end,
    desc = "Search all buffers"
  }
  maps.n["<leader>sh"] = { function() require("telescope.builtin").help_tags() end, desc = "Search help" }
  maps.n["<leader>sm"] = { function() require("telescope.builtin").marks() end, desc = "Search marks" }
  maps.n["<leader>so"] = { function() require("telescope.builtin").oldfiles() end, desc = "Search old files" }
  maps.n["<leader>sw"] = { function() require("telescope.builtin").grep_string() end, desc = "Search word under cursor" }
  maps.n["<leader>ff"] = { function() require("telescope.builtin").find_files() end, desc = "Find files" }
  maps.n["<leader>pf"] = { function() require("telescope.builtin").find_files() end, desc = "Find files" }
  maps.n["<leader>fF"] = {
    function() require("telescope.builtin").find_files { hidden = true, no_ignore = true } end,
    desc = "Find all files",
  }
  maps.n["<leader>bb"] = { function() require("telescope.builtin").buffers() end, desc = "Switch buffers" }
  maps.n["<leader>gg"] = { function() require("telescope.builtin").git_status() end, desc = "Git status" }
  maps.n["<leader>gb"] = { function() require("telescope.builtin").git_branches() end, desc = "Git branches" }
  maps.n["<leader>gc"] = { function() require("telescope.builtin").git_commits() end, desc = "Git commits" }
  maps.n["<leader>cs"] = {
    function()
      local aerial_ok, _ = pcall(require, "aerial")
      if aerial_ok then
        require("telescope").extensions.aerial.aerial()
      else
        require("telescope.builtin").lsp_document_symbols()
      end
    end,
    desc = "Search symbols",
  }
  maps.n["<leader>cD"] = { function() require("telescope.builtin").diagnostics() end, desc = "Find diagnostics" }
  maps.n["gr"] = { function() require("telescope.builtin").lsp_references() end, desc = "Find references" }
  maps.n["gd"] = { function() require("telescope.builtin").lsp_definitions() end, desc = "Show the definition of current symbol" }
  maps.n["gs"] = { function() require("telescope.builtin").lsp_workspace_symbols() end, desc = "Show the workplace's symbols" }
  maps.n["gI"] = { function() require("telescope.builtin").lsp_implementations() end, desc = "Implementation of current symbol" }


  -- Note taking
  if packer.check("telekasten.nvim") then
    maps.n["<leader>vf"] = { function() require("telekasten").find_notes() end, desc = "Find notes" }
    maps.n["<leader>vd"] = { function() require("telekasten").find_daily_notes() end, desc = "Find daily notes" }
    maps.n["<leader>vw"] = { function() require("telekasten").find_weekly_notes() end, desc = "Find weekly notes" }
    maps.n["<leader>va"] = { function() require("telekasten").show_tags() end, desc = "Find tags" }
    maps.n["<leader>vs"] = { function() require("telekasten").search_notes() end, desc = "Search notes" }
    maps.n["<leader>vb"] = { function() require("telekasten").show_backlinks() end, desc = "Note back link" }
    maps.n["<leader>vl"] = { function() require("telekasten").follow_link() end, desc = "Note follow link" }
    maps.n["<leader>vT"] = { function() require("telekasten").goto_today() end, desc = "Go to today note" }
    maps.n["<leader>vW"] = { function() require("telekasten").goto_thisweek() end, desc = "Go to this week" }
    maps.n["<leader>vn"] = { function() require("telekasten").new_note() end, desc = "New note" }
    maps.n["<leader>vN"] = { function() require("telekasten").new_templated_note() end, desc = "New templated note" }
    maps.n["<leader>vr"] = { function() require("telekasten").rename_note() end, desc = "Rename note" }
    maps.n["<leader>vt"] = { function() require("telekasten").toggle_todo() end, desc = "Toggle todo" }
    maps.n["<leader>vc"] = { function() require("telekasten").show_calendar() end, desc = "Show calendar" }
    maps.n["<leader>vC"] = { "<cmd>CalendarT<CR>", desc = "Show calendar" }
    maps.n["<leader>vv"] = { function() require("telekasten").panel() end, desc = "Notes panels" }
  end
end

-- Toggleterm
if packer.check("toggleterm.nvim") then
  local toggle_term_cmd = painlessvim.toggle_term_cmd
  maps.n["<leader>tt"] = { "<cmd>ToggleTerm<cr>", desc = "Toggle terminal" }
  maps.n["<leader>tg"] = { function() toggle_term_cmd "lazygit" end, desc = "Toggle terminal lazygit" }
  maps.n["<leader>tp"] = { function() toggle_term_cmd "python" end, desc = "Toggle terminal python" }
  maps.n["<leader>to"] = { function() toggle_term_cmd "htop" end, desc = "Toggle terminal htop" }
  maps.n["<leader>tf"] = { "<cmd>ToggleTerm direction=float<cr>", desc = "Toggle terminal float" }
  maps.n["<leader>th"] = { "<cmd>ToggleTerm size=10 direction=horizontal<cr>", desc = "Toggle terminal horizontally" }
  maps.n["<leader>tv"] = { "<cmd>ToggleTerm size=80 direction=vertical<cr>", desc = "Toggle terminal vertically" }
end

-- Async Tasks
if packer.check("asynctasks.vim") then
  maps.n["<leader>cl"] = { function() require('telescope').extensions.asynctasks.all() end, desc = "List all project tasks" }
  maps.n["<leader>cc"] = { "<cmd>AsyncTask project-build<cr>", desc = "Project build" }
end

-- INSERT --
--

-- Basic
maps.i["jk"] = { "<esc>", desc = "Normal mode" }

-- VISUAL --
--

-- Basic
maps.v["J"] = { ":m '>+1<CR>gv=gv", desc = "Move current selection down" }
maps.v["K"] = { ":m '<-2<CR>gv=gv", desc = "Move current selection up" }
maps.v["H"] = { "^", desc = "Begin line of text" }
maps.v["L"] = { "$", desc = "End line of text" }
maps.v["<"] = { "<gv", desc = "Unindent line" }
maps.v[">"] = { ">gv", desc = "Indent line" }

-- TERMINAL --
--

-- Basic
maps.t["<esc>"] = { "<C-\\><C-n>", desc = "Terminal normal mode" }
maps.t["jk"] = { "<C-\\><C-n>", desc = "Terminal normal mode" }

painlessvim.set_binding(maps)
