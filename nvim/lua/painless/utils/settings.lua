-- A file by:
--  ___              _    _                 ___          _
-- | _ \ __ _  _ _  | |_ | | ___  ___ ___  / __| ___  __| | ___  _ _
-- |  _// _` || ' \ |  _|| |/ -_)(_-<(_-< | (__ / _ \/ _` |/ -_)| '_|
-- |_|  \__,_||_||_| \__||_|\___|/__//__/  \___|\___/\__,_|\___||_|
--
-- Nghia Lam (Pantless Coder)
-- https://codeberg.org/pantlesscoder/dotfiles

--
-- utils/settings.lua

local cmd = vim.cmd
local opt = vim.opt
local g = vim.g
local indent = 2

cmd([[filetype plugin indent on]])

-- leader key
g.mapleader = ' '

-- misc
opt.backspace = { 'eol', 'start', 'indent' }
opt.clipboard = 'unnamedplus'                      -- Connection to the system clipboard.
opt.encoding = 'utf-8'                             -- File content encoding for the buffer.
opt.matchpairs = { '(:)', '{:}', '[:]', '<:>' }    -- Define the matching pairs.
opt.syntax = 'enable'                              -- Enable all the syntax support.

-- indention
opt.autoindent = true      -- Enable auto indent when working in vim.
opt.expandtab = true       -- Enable the use of space in tab.
opt.smartindent = true     -- Auto detect indentation.
opt.shiftwidth = indent    -- Number of space inserted for indentation.
opt.softtabstop = indent   -- Number of space in a tab when pressing tab.
opt.tabstop = indent       -- Number of space in an actual tab.

-- search
opt.hlsearch = false
opt.ignorecase = true
opt.smartcase = true
opt.wildignore = opt.wildignore + { '*/node_modules/*', '*/.git/*', '*/vendor/*' , '*/vendor/*' }
opt.wildmenu = true

-- ui
opt.cursorline = true        -- Highlight current line.
opt.laststatus = 2           -- Global status.
opt.lazyredraw = true        -- Lazily redraw screen
opt.list = true
opt.listchars = {
  tab = '❘-',
  trail = '·',
  lead = '·',
  extends = '»',
  precedes = '«',
  nbsp = '×',
}
opt.mouse = 'a'              -- Enable mouse support.
opt.number = true            -- Show the line number.
opt.rnu = true               -- Relative line number mode.
opt.scrolloff = 10           -- Number of lines to keep above and below the cursor.
opt.showmode = false         -- Disable showing modes in command line.
opt.sidescrolloff = 3        -- Lines to scroll horizontally.
opt.signcolumn = 'yes'       -- Always show the sign column
opt.splitright = true        -- Open new split to the right.
opt.wrap = false             -- Disable wrapping of lines longer than the width of window.

-- backups
opt.backup = false
opt.swapfile = false
opt.writebackup = false

-- autocomplete
opt.completeopt = { 'menu', 'menuone', 'noselect' }
opt.shortmess = opt.shortmess + { c = true }

-- perfomance
-- opt.redrawtime = 1500
opt.timeoutlen = 500    -- Length of time to wait for a mapped sequence
opt.updatetime = 300    -- Length of time to wait before triggering the plugin

-- theme
opt.termguicolors = true
