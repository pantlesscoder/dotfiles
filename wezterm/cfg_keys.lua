local wezterm = require "wezterm"
local act = wezterm.action

-- NOTE: always use wezterm.action_callback when it's in a release version!!
-- PR (by me!): https://github.com/wez/wezterm/pull/1151
local function act_callback(event_id, callback)
  if wezterm.action_callback then
    wezterm.log_info(">> wezterm.action_callback is available for this version, use it!")
    return wezterm.action_callback(callback)
  else
    wezterm.log_info(">> wezterm.action_callback is NOT available for this version, fallback to manual setup..")
    wezterm.on(event_id, callback)
    return wezterm.action{EmitEvent=event_id}
  end
end

-- IDEA: A better action syntax, see: https://github.com/wez/wezterm/issues/1150

-- IDEA: helper for keybind definition
local function keybind(mods, key, action)
  return {mods = mods, key = key, action = action}
end
local ctrl_shift = "CTRL|SHIFT"

local cfg = {}

cfg.disable_default_key_bindings = true

-- NOTE: About SHIFT and the keybind definition:
-- * For bindings with SHIFT and a letter, the `key` field (the letter)
--   can be lowercase and the mods should NOT contain 'SHIFT'.
-- * For bindings with SHIFT and something else, mod should contain SHIFT,
--   and key should be the shifted key that is going to reach the terminal.
--   (based on the keyboard-layout)
cfg.keys = {
  keybind("SHIFT", "PageUp", act{ScrollByPage = -1}),
  keybind("SHIFT", "PageDown", act{ScrollByPage = 1}),

  -- Wezterm features
  keybind(ctrl_shift, "r", "ReloadConfiguration"),
  -- keybind(ctrl_shift, "l", act{ClearScrollback = "ScrollbackAndViewport"}),
  -- keybind(ctrl_shift, "f", act{Search = {CaseInSensitiveString = ""}}),
  keybind(ctrl_shift, " ", "QuickSelect"),
  keybind("CTRL|ALT", " ", "QuickSelect"), -- note: eats a valid terminal keybind
  keybind(ctrl_shift, "d", "ShowDebugOverlay"), -- note: it's not a full Lua interpreter

  keybind(ctrl_shift, "f", "ToggleFullScreen"),

  -- Copy/Paste to/from Clipboard
  keybind(ctrl_shift, "c", act{CopyTo = "Clipboard"}),
  keybind(ctrl_shift, "v", act{PasteFrom = "Clipboard"}),

  -- Paste from PrimarySelection (Copy is done by selection)
  keybind("SHIFT",    "Insert", act{PasteFrom = "PrimarySelection"}),
  keybind("CTRL|ALT", "v",      act{PasteFrom = "PrimarySelection"}),
  -- NOTE: the last one eats a valid terminal keybind

  -- Tabs
  keybind(ctrl_shift, "t", act{SpawnTab="DefaultDomain"}),
  keybind("CTRL",     "Tab", act{ActivateTabRelative=1}),
  keybind(ctrl_shift, "Tab", act{ActivateTabRelative=-1}),
  keybind(ctrl_shift, "w", act{CloseCurrentTab={confirm=false}}),

  -- Pane
  keybind(ctrl_shift, "6", act{SplitVertical={domain="CurrentPaneDomain"}}),
  keybind(ctrl_shift, "5", act{SplitHorizontal={domain="CurrentPaneDomain"}}),
  keybind(ctrl_shift, "x", act{CloseCurrentPane={confirm=true}}),
  keybind(ctrl_shift, "h", act{AdjustPaneSize={"Left", 5}}),
  keybind(ctrl_shift, "l", act{AdjustPaneSize={"Right", 5}}),
  keybind(ctrl_shift, "j", act{AdjustPaneSize={"Down", 5}}),
  keybind(ctrl_shift, "k", act{AdjustPaneSize={"Up", 5}}),
  keybind("CTRL",     "h", act{ActivatePaneDirection="Left"}),
  keybind("CTRL",     "l", act{ActivatePaneDirection="Right"}),
  keybind("CTRL",     "j", act{ActivatePaneDirection="Down"}),
  keybind("CTRL",     "k", act{ActivatePaneDirection="Up"}),

  -- Font size
  keybind(ctrl_shift, "0", "ResetFontSize"), -- Ctrl-Shift-0
  keybind(ctrl_shift, ">", "IncreaseFontSize"), -- Ctrl - Shift - >
  keybind(ctrl_shift, "<", "DecreaseFontSize"), -- Ctrl - Shift - < (key with -)

  ---- Custom events

  keybind(ctrl_shift, "g", act_callback("toggle-ligatures", function(win, _)
    local overrides = win:get_config_overrides() or {}
    if not overrides.harfbuzz_features then
      -- If we haven't overriden it yet, then override with ligatures disabled
      overrides.harfbuzz_features = {"calt=0", "clig=0", "liga=0"}
    else
      -- else we did already, and we should disable the override now
      overrides.harfbuzz_features = nil
    end
    win:set_config_overrides(overrides)
  end)),
}

return cfg
