local col = {}

-- Bew colors
col.background = "#2b3339"
col.foreground = "#d3c6aa"

col.cursor_bg = "#d3c6aa"
col.cursor_fg = "#202020"
col.cursor_border = "#d3c6aa" -- same as cursor_bg

col.ansi = {
  "#2F2F2F", -- black
  "#e67e80", -- red
  "#a7c080", -- green
  "#dbbc7f", -- yellow
  "#7fbbb3", -- blue
  "#e69875", -- orange (magentas usually)
  "#83c092", -- cyan
  "#cccccc", -- white
}

col.brights = {
  "#555753", -- black
  "#e67e80", -- red
  "#a7c080", -- green
  "#dbbc7f", -- yellow
  "#7fbbb3", -- blue
  "#e69875", -- orange (magentas usually)
  "#83c092", -- cyan
  "#fafafa", -- white
}

col.tab_bar = {

  background = "#005f5f",

  -- The active tab is the one that has focus in the window
  active_tab = {
    -- The color of the background area for the tab
    bg_color = col.background,
    -- The color of the text for the tab
    fg_color = col.foreground,

    -- Specify whether you want "Half", "Normal" or "Bold" intensity for the
    -- label shown for this tab.
    -- The default is "Normal"
    intensity = "Normal",

    -- Specify whether you want "None", "Single" or "Double" underline for
    -- label shown for this tab.
    -- The default is "None"
    underline = "None",

    -- Specify whether you want the text to be italic (true) or not (false)
    -- for this tab.  The default is false.
    italic = false,

    -- Specify whether you want the text to be rendered with strikethrough (true)
    -- or not for this tab.  The default is false.
    strikethrough = false,
  },

  new_tab = {
    bg_color = "#005f5f",
    fg_color = "#ffffff",
    bold = true,

    -- The same options that were listed under the `active_tab` section above
    -- can also be used for `new_tab_hover`.
  },

  -- -- Inactive tabs are the tabs that do not have focus
  -- inactive_tab = {
  --   bg_color = colors.bg,
  --   fg_color = colors.fg,

  --   -- The same options that were listed under the `active_tab` section above
  --   -- can also be used for `inactive_tab`.
  -- },
}

return col
