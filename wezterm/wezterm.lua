-- WezTerm configuration
---------------------------------------------------------------

local mytable = require "lib/mystdlib".mytable
local wezterm = require 'wezterm';

-- Misc
------------------------------------------
local tab_max_width = 20

local cfg_misc = {
  window_close_confirmation = "NeverPrompt",
  check_for_updates = true,

  -- Use the powershell as default program
  -- default_prog = {"C:/Program Files/WindowsApps/Microsoft.PowerShell_7.2.4.0_x64__8wekyb3d8bbwe/pwsh.exe", "-NoLogo"},

  -- We can disable this if we want to avoid unexpected config breakage and
  -- unusable terminal
  automatically_reload_config = true,

  -- Make sure word selection stops on most punctuations.
  -- Note that dot (.) & slash (/) are allowed though for
  -- easy selection of paths.
  selection_word_boundary = " \t\n{}[]()\"'`,;:@",

  hide_tab_bar_if_only_one_tab = true,
  -- window_decorations = "TITLE",

  -- Do not hold on exit by default.
  -- Because the default 'CloseOnCleanExit' can be annoying when exiting with
  -- Ctrl-D and the last command exited with non-zero: the shell will exit
  -- with non-zero and the terminal would hang until the window is closed manually.
  exit_behavior = "Close",

  -- Pad window to avoid the content to be too close to the border,
  -- so it's easier to see and select.
  window_padding = {
    left = 5, right = 3,
    top = 5, bottom = 2,
  },

  -- cf the original issue (mine): https://github.com/wez/wezterm/issues/478 solved for me but not for everyone..
  -- cf the addition of this flag: https://github.com/wez/wezterm/commit/336f209ede27dd801f989419155e475f677e8244
  -- OK BUT NO, disabled because it does some weird visual artifacts:
  --  * About cursor behaviors:
  --    When a ligature is a the end of the line & the nvim' window
  --    is a little bit larger than the text so that when the cursor comes
  --    closer to the window border (and on the ligature), the buffer does
  --    a side-scroll. Then the cursor does wonky stuff when moving w.r.t that
  --    end-of-line ligature.
  --
  --  * About some symbols display:
  --    The git above/below arrows on the right of my prompt.
  --
  -- experimental_shape_post_processing = true,

  tab_max_width = tab_max_width,
  tab_bar_at_bottom = false,
  -- intentionally both empty as we use the title bar function
  --tab_bar_style = {
    --active_tab_left = empty,
    --active_tab_right = empty,
    --inactive_tab_left = empty,
    --inactive_tab_right = empty,
    --inactive_tab_hover_left = empty,
    --inactive_tab_hover_right = empty,
  --},
}

-- Colors & Appearance
------------------------------------------

local cfg_colors = {
  colors = require("cfg_bew_colors"),
  color_scheme = col
}

-- Font
------------------------------------------

local cfg_fonts = require("cfg_fonts")

-- Key/Mouse bindings
------------------------------------------

-- Key bindings
local cfg_key_bindings = require("cfg_keys")

-- Mouse bindings
local cfg_mouse_bindings = require("cfg_mouse")

wezterm.on("update-right-status", function(window, pane)
  -- Each element holds the text for a cell in a "powerline" style << fade
  local cells = {};

  -- Figure out the cwd and host of the current pane.
  -- This will pick up the hostname for the remote host if your
  -- shell is using OSC 7 on the remote host.
  local cwd_uri = pane:get_current_working_dir()
  if cwd_uri then
    cwd_uri = cwd_uri:sub(8);
    local slash = cwd_uri:find("/")
    local cwd = ""
    local hostname = ""
    if slash then
      hostname = cwd_uri:sub(1, slash-1)
      -- Remove the domain name portion of the hostname
      local dot = hostname:find("[.]")
      if dot then
        hostname = hostname:sub(1, dot-1)
      end
      -- and extract the cwd from the uri
      cwd = cwd_uri:sub(slash)

      table.insert(cells, cwd);
      table.insert(cells, hostname);
    end
  end

  -- I like my date/time in this style: "Wed Mar 3 08:14"
  local date = wezterm.strftime("%a %b %-d %H:%M");
  table.insert(cells, date);

  -- An entry for each battery (typically 0 or 1 battery)
  for _, b in ipairs(wezterm.battery_info()) do
    table.insert(cells, string.format("%.0f%%", b.state_of_charge * 100))
  end

  -- Color palette for the backgrounds of each cell
  local colors = {
    "#00af87",
    "#dbbc7f",
    "#7fbbb3",
    "#e69875",
    "#83c092",
  };

  -- Foreground color for the text across the fade
  local text_fg = "#ffffff";

  -- The elements to be formatted
  local elements = {};
  -- How many cells have been formatted
  local num_cells = 0;

  -- Translate a cell into elements
  function push(text, is_last)
    local cell_no = num_cells + 1
    table.insert(elements, {Foreground={Color=text_fg}})
    table.insert(elements, {Background={Color=colors[cell_no]}})
    table.insert(elements, {Text=" "..text.." "})
    if not is_last then
      table.insert(elements, {Foreground={Color=colors[cell_no+1]}})
      --table.insert(elements, {Text=SOLID_LEFT_ARROW})
    end
    num_cells = num_cells + 1
  end

  while #cells > 0 do
    local cell = table.remove(cells, 1)
    push(cell, #cells == 0)
  end

  window:set_right_status(wezterm.format(elements));
end);

-- Merge configs and return!
------------------------------------------
local config = mytable.merge_all(
  cfg_misc,
  cfg_colors,
  cfg_fonts,
  cfg_key_bindings,
  cfg_mouse_bindings
)

return config
