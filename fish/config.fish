# A file by:

#  ___              _    _                 ___          _
# | _ \ __ _  _ _  | |_ | | ___  ___ ___  / __| ___  __| | ___  _ _
# |  _// _` || ' \ |  _|| |/ -_)(_-<(_-< | (__ / _ \/ _` |/ -_)| '_|
# |_|  \__,_||_||_| \__||_|\___|/__//__/  \___|\___/\__,_|\___||_|
#
# Nghia Lam (Pantless Coder)
# https://codeberg.org/pantlesscoder/dotfiles

#
# ~/config.fish

####################################################################
# Settings
####################################################################

if status is-interactive
    # Commands to run in interactive sessions can go here
end

function fish_greeting
    echo Hello,
    echo " ___              _    _                 ___          _"
    echo "| _ \ __ _  _ _  | |_ | | ___  ___ ___  / __| ___  __| | ___  _ _"
    echo "|  _// _` || \ \ |  _|| |/ -_)(_-<(_-< | (__ / _ \/ _` |/ -_)| \_|"
    echo "|_|  \__,_||_||_| \__||_|\___|/__//__/  \___|\___/\__,_|\___||_|"
    echo
    echo Current time is (set_color yellow; date +%T; set_color normal), lets have a nice day!!
    echo
    echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~^O^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    echo
end

####################################################################
# Export
####################################################################
set TERM "xterm-256color"                      # Sets the terminal type
set EDITOR "emacsclient -t -a ''"              # $EDITOR use Emacs in terminal
set VISUAL "emacsclient -c -a emacs"           # $VISUAL use Emacs in GUI mode

####################################################################
# Alias
####################################################################

# navigation
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

# vim and emacs
alias vm='nvim'
alias vim='nvim'
alias emacs="emacsclient -c -a 'emacs'"
alias doominstall="~/.emacs.d/bin/doom install"
alias doomsync="~/.emacs.d/bin/doom sync"
alias doomdoctor="~/.emacs.d/bin/doom doctor"
alias doomupgrade="~/.emacs.d/bin/doom upgrade"
alias doompurge="~/.emacs.d/bin/doom purge"

# git
alias gpush="git push"
alias gpull="git pull"
alias lg="lazygit"

# Changing "ls" to "exa"
alias ls='exa -al --color=always --group-directories-first --icons' # my preferred listing
alias la='exa -a --color=always --group-directories-first --icons'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first --icons'  # long format
alias lt='exa -aT --color=always --group-directories-first --icons' # tree listing
alias l.='exa -a | egrep "^\."'

# pacman and yay
alias pacsyu='sudo pacman -Syu'                  # update only standard pkgs
alias pacsyyu='sudo pacman -Syyu'                # Refresh pkglist & update standard pkgs
alias yaysua='yay -Sua --noconfirm'              # update only AUR pkgs (yay)
alias yaysyu='yay -Syu --noconfirm'              # update standard pkgs and AUR pkgs (yay)
alias parsua='paru -Sua --noconfirm'             # update only AUR pkgs (paru)
alias parsyu='paru -Syu --noconfirm'             # update standard pkgs and AUR pkgs (paru)
alias unlock='sudo rm /var/lib/pacman/db.lck'    # remove pacman lock
alias cleanup='sudo pacman -Rns (pacman -Qtdq)'  # remove orphaned packages
