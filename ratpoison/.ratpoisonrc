# A file by:
#  ___              _    _                 ___          _
# | _ \ __ _  _ _  | |_ | | ___  ___ ___  / __| ___  __| | ___  _ _
# |  _// _` || ' \ |  _|| |/ -_)(_-<(_-< | (__ / _ \/ _` |/ -_)| '_|
# |_|  \__,_||_||_| \__||_|\___|/__//__/  \___|\___/\__,_|\___||_|
#
# Nghia Lam (Pantless Coder)
# https://codeberg.org/pantlesscoder/dotfiles

#
# .ratpoisonrc - My ratpoison window manager configs

############################################################################
# Default settings
############################################################################

exec /usr/bin/rpws 5 -k                     # Create 5 workspaces
exec ratpoison -c 'set winfmt %n %a %s %t'  # Set window name
exec sleep 1; ratpoison -c "grename Home"   # Make default group
exec ratpoison -c 'banish'                  # Isolate the mouse, who needs it, right?
exec /usr/bin/emacs --daemon                # Init emacs server

# Startup message
exec sleep 1; ratpoison -c "echo Welcome, Pantless Coder!!"

# Uncomment this if I ever need to use the mouse, like seriously ...
# exec /usr/share/ratpoison/sloppy                            # Follow mouse

set warp 0
set startupmessage 0
set winname title
set wingravity center
set transgravity center
set border 3
set padding 0 0 0 0
set barborder 3
set barpadding 10 10
set barinpadding 0
set inputwidth 200
set bargravity c
set mesgwait 4
# set framemsgwait -1

set font "Hurmit Nerd Font:size=14"
set bgcolor #000000
set fgcolor #55af66
set fwcolor #55af66
set bwcolor #224528

############################################################################
# Key binding
############################################################################

bind escape abort

bind e exec emacsclient -c -a emacs
bind t exec st

# Frames
bind f exec ratpoison -i -c fselect
bind r exec ratpoison -i -c resize

definekey top M-l focusright
definekey top M-h focusleft
definekey top M-k focusup
definekey top M-j focusdown
definekey top M-L exchangeright
definekey top M-H exchangeleft
definekey top M-K exchangeup
definekey top M-J exchangedown

bind R exec ratpoison -c remove && rp_autoborder.sh &
bind s exec ratpoison -c hsplit && rp_autoborder.sh &
bind S exec ratpoison -c split && rp_autoborder.sh &
bind o exec ratpoison -c only && rp_autoborder.sh &

# Groups
# bind x exec rp_tabbedrename.sh &
bind X exec ratpoison -i -c grename
bind g exec ratpoison -i -c groups
bind G exec ratpoison -i -c gselect
bind D exec ratpoison -c "gdelete $(ratpoison -c "prompt gdelete: ")" &
bind N exec ratpoison -c gnext
bind S-ISO_Left_Tab exec ratpoison -c gother
bind P exec ratpoison -c gprev

# Windows
bind w exec ratpoison -i -c "windows %n %a: %t"
bind m exec ratpoison -i -c gmove
bind M exec ratpoison -i -c gmerge
bind d exec ratpoison -c dedicate

# Launcher
bind d exec dmenu_run
bind D exec $(dmenu_path | dmenu -fn fixed -nb black -nf green -sb green -sf black)
bind exclam exec ratpoison -c "exec $(ratpoison -c "prompt exec: ")" &
