#!/usr/bin/env bash

source ~/.config/ratpoison/.ratpoisonvar

FRAMECOUNT=`ratpoison -c fdump | tr ',' '\n' | wc -l`
CHECKBAR=`pgrep dzen2 | wc -l`

if [ $FRAMECOUNT == 1 ] && [ $CHECKBAR == 0 ]; then
    ratpoison -c "set fwcolor $rpcolor_0"
else
    ratpoison -c "set fwcolor $rpcolor_1"
fi
